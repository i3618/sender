package storage

import (
	"tasks/Telegram_Message_Sender/sender/storage/postgres"
	"tasks/Telegram_Message_Sender/sender/storage/repo"

	"github.com/jmoiron/sqlx"
)

type IStorage interface {
	Bot() repo.SendPostgres
}
type storagePg struct {
	db       *sqlx.DB
	sendRepo repo.SendPostgres
}

func NewStoragePg(db *sqlx.DB) *storagePg {
	return &storagePg{
		db:       db,
		sendRepo: postgres.NewBotRepo(db),
	}
}

func (s storagePg) Bot() repo.SendPostgres {
	return s.sendRepo
}
