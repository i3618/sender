package postgres

import (
	"database/sql"
	"fmt"
	sp "tasks/Telegram_Message_Sender/sender/genproto"
	"time"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type SendRepo struct {
	db *sqlx.DB
}

func NewBotRepo(db *sqlx.DB) *SendRepo {
	return &SendRepo{db: db}
}

func (r *SendRepo) InsertMess(req *sp.MessageReq) error {
	var (
		NewId uuid.UUID
	)
	query := `INSERT INTO messages (id, message, created_at, priority) VALUES($1,$2,$3,$4)`

	NewId = uuid.New()

	_, err := r.db.Exec(query, NewId, req.Text, req.CreatedAt, req.Priority)
	if err != nil {
		return err
	}

	return nil
}
func (r *SendRepo) SendedMess(Id string) error {
	query := `UPDATE messages SET sended_at=$2 WHERE id=$1 AND sended_at IS NULL`
	Now := time.Now().Format(time.RFC3339)
	_, err := r.db.Exec(query, Id, Now)
	if err != nil {
		return err
	}
	return nil
}
func (r *SendRepo) GetMess() (*sp.Res, error) {
	query := `SELECT id, message FROM messages WHERE sended_at IS NULL ORDER BY priority ASC, created_at ASC LIMIT 1`
	res := sp.Res{}
	err := r.db.QueryRow(query).Scan(
		&res.Id,
		&res.Message,
	)
	if err != nil && err != sql.ErrNoRows {
		fmt.Println("err1", err)
		return nil, err
	} else if err == sql.ErrNoRows {
		return nil, nil
	}

	return &res, nil
}

func (r *SendRepo) InsertId(ChatId int64) error {
	query := `INSERT INTO chats_id (id, chats) VALUES ($1,$2)`
	id := uuid.New()
	_, err := r.db.Exec(query, id, ChatId)
	if err != nil {
		return err
	}
	return nil
}
func (r *SendRepo) GetChatId() ([]int64, error) {
	var chats []int64
	query := `SELECT chats FROM chats_id`

	rows, err := r.db.Query(query)
	if err != nil && err != sql.ErrNoRows {
		fmt.Println("Error while if 78", err)
		return chats, err
	} else if err == sql.ErrNoRows {
		return chats, nil
	}

	for rows.Next() {
		var chatId int64
		err = rows.Scan(
			&chatId,
		)
		if err != nil && err != sql.ErrNoRows {
			fmt.Println("Error while if 91", err)
			return chats, err
		} else if err == sql.ErrNoRows {
			return chats, nil
		}
		chats = append(chats, chatId)
	}
	return chats, nil
}
func (r *SendRepo) DeleteChatId(id int64) error {
	query := `DELETE FROM chats_id WHERE chats=$1`
	_, err := r.db.Exec(query, id)
	if err != nil {
		return err
	}
	return nil
}
