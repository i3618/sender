package repo

import (
	sp "tasks/Telegram_Message_Sender/sender/genproto"
)

type SendPostgres interface {
	InsertMess(*sp.MessageReq) error
	GetMess() (*sp.Res, error)
	SendedMess(Id string) error
	InsertId(ChatId int64) error
	GetChatId() ([]int64, error)
	DeleteChatId(id int64) error
}
