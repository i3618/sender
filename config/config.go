package config

import (
	"os"

	"github.com/spf13/cast"
)

// Config ...
type Config struct {
	Environment string

	PostgresHost     string
	PostgresPort     int
	PostgresDatabase string
	PostgresUser     string
	PostgresPassword string
	BotToken string

	LogLevel string
	Port     string
}

// Load loads environment vars and inflates Config
func Load() Config {
	c := Config{}

	c.Environment = cast.ToString(GetFromOs("ENVIRONMENT", "develop"))
	c.BotToken = cast.ToString(GetFromOs("BOT_TOKEN","5135738281:AAGm4qgy3M8ui7ebwgFjdek9QCN_YCovZFM"))
	c.PostgresHost = cast.ToString(GetFromOs("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToInt(GetFromOs("POSTGRES_PORT", 5432))
	c.PostgresDatabase = cast.ToString(GetFromOs("POSTGRES_DATABASE", "info_bot"))
	c.PostgresUser = cast.ToString(GetFromOs("POSTGRES_USER", "khusniddin"))
	c.PostgresPassword = cast.ToString(GetFromOs("POSTGRES_PASSWORD", "1234"))
	c.LogLevel = cast.ToString(GetFromOs("LOG_LEVEL", "debug"))

	c.Port = cast.ToString(GetFromOs("PORT", ":9000"))

	return c
}

// Get from os (if Exists)
func GetFromOs(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}

	return defaultValue
}
