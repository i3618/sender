CURRENT_DIR=$(shell pwd)

proto-gen:
	./scripts/proto-gen.sh ${CURRENT_DIR}
	ls genproto/ | xargs -n1 -IX bash -c "sed -e '/bool/ s/,omitempty//' X > X.tmp && mv X{.tmp,}"
mig-create:
	migrate create -ext sql -dir migrations -seq create_chats_id_table

mig-up:
	migrate -source file:./migrations -database 'postgres://khusniddin:1234@localhost:5432/info_bot?sslmode=disable' up

mig-down:
	migrate -source file:./migrations -database 'postgres://khusniddin:1234@localhost:5432/info_bot?sslmode=disable' down
