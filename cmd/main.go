package main

import (
	"net"
	"tasks/Telegram_Message_Sender/sender/config"
	sp "tasks/Telegram_Message_Sender/sender/genproto"
	"tasks/Telegram_Message_Sender/sender/pkg/db"
	"tasks/Telegram_Message_Sender/sender/pkg/logger"
	"tasks/Telegram_Message_Sender/sender/service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	config := config.Load()

	log := logger.New(config.LogLevel, "sender_service")
	defer logger.Cleanup(log)

	log.Info("main: sqlxConfig",
		logger.String("host", config.PostgresHost),
		logger.Int("port", config.PostgresPort),
		logger.String("database", config.PostgresDatabase),
		logger.String("password", config.PostgresPassword))
	connDB, err := db.ConnectToDB(config)
	if err != nil {
		log.Fatal("sqlx connection to postgres error", logger.Error(err))
	}

	senderService := service.NewSenderService(connDB, log)

	lis, err := net.Listen("tcp", config.Port)
	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

	s := grpc.NewServer()
	sp.RegisterSenderServiceServer(s, senderService)
	reflection.Register(s)

	log.Info("main: server running",
		logger.String("port", config.Port))

	go senderService.Sender()

	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
}
