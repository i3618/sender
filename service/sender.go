package service

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"tasks/Telegram_Message_Sender/sender/config"
	sp "tasks/Telegram_Message_Sender/sender/genproto"
	l "tasks/Telegram_Message_Sender/sender/pkg/logger"
	"tasks/Telegram_Message_Sender/sender/storage"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/jmoiron/sqlx"
)

type BotService struct {
	storage storage.IStorage
	logger  l.Logger
}

func NewSenderService(db *sqlx.DB, log l.Logger) *BotService {
	return &BotService{
		storage: storage.NewStoragePg(db),
		logger:  log,
	}
}

func (r *BotService) SendMessage(ctx context.Context, req *sp.MessageReq) (*sp.Res, error) {
	err := r.storage.Bot().InsertMess(req)
	if err != nil {
		r.logger.Error("Error: ", l.Error(err))
		return nil, err
	}
	return &sp.Res{}, nil
}

func (r *BotService) Sender() {

	var (
		BotApi   = "https://api.telegram.org/bot"
		BotToken = config.Load().BotToken
		BotUrl   = BotApi + BotToken
	)
	bot, err := tgbotapi.NewBotAPI(BotToken)
	if err != nil {
		fmt.Println(69, "- line")
		log.Panic(err)
	}
	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := bot.GetUpdatesChan(u)

	var chats_id []int64
	res, err := r.storage.Bot().GetChatId()
	if err != nil {
		fmt.Println("84-line", err)
		return
	}
	chats_id = res

	go r.Send(&chats_id, BotUrl, bot)

	for update := range updates {
		short := update.MyChatMember

		if short == nil {
			continue
		} else if short.Chat.Type == "group" && short.NewChatMember.Status != "left" {
			if !(Contains(chats_id, short.Chat.ID)) {
				err = r.storage.Bot().InsertId(short.Chat.ID)
				if err != nil {
					fmt.Println("99-line", err)
					return
				}
				res, err := r.storage.Bot().GetChatId()
				if err != nil {
					fmt.Println("104-line", err)
					return
				}
				chats_id = res
			}
		} else if short.NewChatMember.Status == "left" {
			if Contains(chats_id, short.Chat.ID) {
				var n []int64
				for _, j := range chats_id {
					if j != short.Chat.ID {
						n = append(n, j)
					}
				}
				chats_id = n
				err = r.storage.Bot().DeleteChatId(short.Chat.ID)
				if err != nil {
					fmt.Println("120-line", err)
					return
				}
			}
		}

		if short.NewChatMember.CanPostMessages && (short.NewChatMember.User.ID == bot.Self.ID) && (short.NewChatMember.Status != "kicked") {
			if !(Contains(chats_id, short.Chat.ID)) {
				err = r.storage.Bot().InsertId(short.Chat.ID)
				if err != nil {
					fmt.Println("131-line", err)
					return
				}
				res, err := r.storage.Bot().GetChatId()
				if err != nil {
					fmt.Println("136-line", err)
					return
				}
				chats_id = res
			}
		} else if short.NewChatMember.Status == "kicked" {
			if Contains(chats_id, short.Chat.ID) {
				var n []int64
				for _, j := range chats_id {
					if j != short.Chat.ID {
						n = append(n, j)
					}
				}
				chats_id = n
				err = r.storage.Bot().DeleteChatId(short.Chat.ID)
				if err != nil {
					fmt.Println("152-line", err)
					return
				}
			}
		}
		// fmt.Println(chats_id)
	}
}
func (r *BotService) Send(chats_id *[]int64, botUrl string, bot *tgbotapi.BotAPI) {
	for {

		Message, err := r.storage.Bot().GetMess()
		// fmt.Println(chats_id, Message)

		if err != nil {
			fmt.Println("167-line", err)
			return
		} else if Message == nil {
			time.Sleep(time.Second * 5)
			continue
		}
		if len(*chats_id) != 0 {
			for _, j := range *chats_id {

				url := botUrl + "/sendMessage?chat_id=" + strconv.Itoa(int(j)) + "&text=" + Message.Message
				// fmt.Println(url)
				_, err = http.Get(url)
				if err != nil {
					fmt.Println("178-line", err)
					return
				}
			}
			r.storage.Bot().SendedMess(Message.Id)
		}
		time.Sleep(time.Second * 5)
	}
}
func Contains(arr []int64, val int64) bool {
	for _, a := range arr {
		if a == val {
			return true
		}
	}
	return false
}
