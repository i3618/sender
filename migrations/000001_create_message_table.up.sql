CREATE TABLE IF NOT EXISTS messages (
    Id UUID,
    Message TEXT,
    Priority INT,
    Created_at TIMESTAMP,
    Sended_at TIMESTAMP
);